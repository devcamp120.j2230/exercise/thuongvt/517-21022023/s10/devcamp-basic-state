import { Component } from "react";

class Info extends Component{

    constructor(props){
        super(props); // thuộc tính của classs
        this.state = { // state là một object được khai báo trong constructor của class component 
            count: 0

        }
    }
// 
    clickChanghandler =()=>{
        this.setState({ // hàm arrow không có thuộc tính this nên this khai báo trong hàm này là của class Info chú ý nhé
            count: this.state.count + 1
        })
    }
    render(){
        return(
            <>
                <p>Count: {this.state.count}</p>
                <button onClick={this.clickChanghandler}>Click here</button>
            </>
        )
    }
}

export default Info